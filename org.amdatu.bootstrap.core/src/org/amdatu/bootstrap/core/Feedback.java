package org.amdatu.bootstrap.core;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface Feedback {
	void println(String message);
}

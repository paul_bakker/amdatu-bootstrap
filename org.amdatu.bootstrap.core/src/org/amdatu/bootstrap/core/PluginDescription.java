package org.amdatu.bootstrap.core;

import java.util.List;

public class PluginDescription {
	private final String m_name;
	private final List<CommandDescription> m_commandDescription;
	
	public PluginDescription(String name, List<CommandDescription> commandDescription) {
		m_name = name;
		m_commandDescription = commandDescription;
	}

	public String getName() {
		return m_name;
	}

	public List<CommandDescription> getCommandDescription() {
		return m_commandDescription;
	}
}

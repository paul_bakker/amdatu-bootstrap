package org.amdatu.bootstrap.core.commands;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
@PluginInfo(name="plugins")
public class BootstrapConfigPlugin implements BootstrapPlugin{
	
	@ServiceDependency
	private volatile PluginRegistry m_registry;
	
	@Command
	public String list() {
		StringBuilder sb = new StringBuilder();
		
		m_registry.listPlugins().forEach(p -> {
			sb.append(p).append("\n");
//			m_registry.getCommands(p).forEach(c -> {
//				sb.append("   -").append(c);
//			});
			
			sb.append("\n\n");
		});
		
		return sb.toString();
	}
}

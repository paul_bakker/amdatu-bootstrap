package org.amdatu.bootstrap.getopt;

import java.util.List;
import java.util.Map;


public interface Options {
	List<?> _();

	CommandLine _command();

	Map<String,String> _properties();

	boolean _ok();

	boolean _help();
}

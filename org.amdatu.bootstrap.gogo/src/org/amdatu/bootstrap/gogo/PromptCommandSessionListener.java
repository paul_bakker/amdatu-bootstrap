package org.amdatu.bootstrap.gogo;

import org.amdatu.bootstrap.core.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.apache.felix.service.command.CommandSession;

@Component
public class PromptCommandSessionListener implements CommandSessionListener {

	@ServiceDependency
	private volatile Navigator m_navigator;

	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1,
			Exception arg2) {
		setPrompt(arg0);
	}

	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1,
			Object arg2) {
		setPrompt(arg0);
	}

	private void setPrompt(CommandSession arg0) {
		String prompt;
		if (m_navigator.getProjectDir() != null){
			prompt = String.format("bootstrap [%s:%s]! ", m_navigator.getProjectDir().getParent().toFile().getName(), 
					m_navigator.getProjectDir().toFile().getName());
		}else if (m_navigator.getWorkspaceDir() != null){
			prompt = String.format("bootstrap [%s]! ", m_navigator.getWorkspaceDir().toFile().getName());
		}else {
			prompt = String.format("bootstrap %s! ", m_navigator.getCurrentDir().toAbsolutePath().toString());
		}
		
		arg0.put("prompt", prompt);
	}

	@Override
	public void beforeExecute(CommandSession arg0, CharSequence arg1) {
		
	}
}
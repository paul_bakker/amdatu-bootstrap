package org.amdatu.bootstrap.plugins.baseline;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.Feedback;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.core.Prompt;
import org.amdatu.bootstrap.core.ResourceManager;
import org.amdatu.bootstrap.getopt.Parameters;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.Project;
import aQute.bnd.build.ProjectBuilder;
import aQute.bnd.build.Workspace;
import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.differ.Baseline;
import aQute.bnd.differ.Baseline.Info;
import aQute.bnd.differ.DiffPluginImpl;
import aQute.bnd.osgi.Constants;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Processor;
import aQute.bnd.properties.Document;
import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component
@PluginInfo(name="baseline")
public class BaselineCommand implements BootstrapPlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile Feedback m_feedback;

	interface BaselineParams extends Parameters {
		boolean fix();
	}
	

	@Command
	public Void baseline(BaselineParams params, Prompt prompt) {
		try {
			//TODO: get diff ignore headers from bnd project and configure differ
			DiffPluginImpl differ = new DiffPluginImpl();
			
			Map<String, List<BaseLineError>> baseLineErrors = new HashMap<>();
	        List<File> generatedJars = getGeneratedJars();
	        
	        for (File jarFile : generatedJars) {
	            try (Jar jar = new Jar(jarFile)) {
	                String bsn = jar.getBsn();
	                Version version = new Version(jar.getVersion());
	                m_feedback.println("Baselining: " + bsn + " " + version);
	                
                    Jar baseLineJar = getBaselineJar(bsn,version);
                    if (baseLineJar != null){
	                    Processor bnd = new Processor();
	                    Baseline baseLine = new Baseline(bnd, differ);
	                    baseLine.baseline(jar, baseLineJar, null);
	                    List<BaseLineError> errors = new ArrayList<>();
	                    if (baseLine.getBundleInfo().mismatch) {
	                        BaseLineError err = new BaseLineError();
	                        err.bsn = bsn;
	                        err.newVersion = baseLine.getSuggestedVersion().toString();
	                        errors.add(err);
	                    } 
	                    for (Info i : baseLine.getPackageInfos()) {
	                        if (i.mismatch) {
	                            BaseLineError err = new BaseLineError();
	                            err.bsn = bsn;
	                            err.packageName = i.packageName;
	                            err.newVersion = baseLine.getSuggestedVersion().toString();
	                            errors.add(err);
	                        }
	                    }
	                    if (!errors.isEmpty()) {
	                        baseLineErrors.put(bsn, errors);
	                    }
		            }
                }
	        }
	            
	        if (!baseLineErrors.isEmpty()) {
	        	m_feedback.println("Baselinine erros:");
	            for (Entry<String, List<BaseLineError>> entry : baseLineErrors.entrySet()) {
	            	m_feedback.println(entry.getKey() + ": ");
	                for (BaseLineError e : entry.getValue()) {
	                	m_feedback.println("  " + e.toString());
	                }
	            }
	            
	            
	    		Boolean fix = prompt.askBoolean("Fix?", false);  
	    		
	            if (fix){
	                fixBaseLineErrors(baseLineErrors);
	            }
	            
	            
	            return null;
	        } else {
	        	m_feedback.println("Baseline OK.");
	            return null;
	        }
	        

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return null;
	}
	
	private void fixBaseLineErrors(Map<String, List<BaseLineError>> baseLineErrors) throws Exception {
        List<File> generatedJars = getGeneratedJars();
        for (File jarFile : generatedJars) {
            try (Jar jar = new Jar(jarFile)) {
                String bsn = jar.getBsn();
                if (baseLineErrors.containsKey(bsn)) {
                    for (BaseLineError err : baseLineErrors.get(bsn)) {
                        if (err.packageName == null) {
                            fixProject(jarFile, bsn, err);
                        } else {
                            fixPackage(jarFile, bsn, err);
                        }
                    }
                }
            }
        }
        m_feedback.println("All done, you might want to trigger a new build to update the generated files.");
    }
	
	
	private void fixPackage(File jarFile, String bsn, BaseLineError err) throws IOException {
	        File projectFolder = jarFile.getParentFile().getParentFile();
	        List<File> packageFiles = m_resourceManager.findFiles(projectFolder, "packageinfo");
	    for (File f : packageFiles) {
	        if (f.getAbsolutePath().replace(File.separatorChar, '.').contains(err.packageName)) {
	            try (PrintWriter out = new PrintWriter(f)){
	                out.print("version " + err.newVersion + "\n");
	            }
	        }
	    }
	}

	private void fixProject(File jarFile, String bsn, BaseLineError err) {
	    File projectFolder = jarFile.getParentFile().getParentFile();
        try {
            Project project = m_navigator.getCurrentWorkspace().getProject(projectFolder.getName());
            
            File bndFile = null;
            if (project.get(Constants.SUB) == null && project.getBuilder(null).getBsn().equals(bsn)){
            	bndFile = project.getPropertiesFile();
            }else{
            	ProjectBuilder subBuilder = project.getSubBuilder(bsn);
            	bndFile = subBuilder.getPropertiesFile();
            }

            BndEditModel model = new BndEditModel();
            model.loadFrom(bndFile);
   
            model.setBundleVersion(err.newVersion);
   
            Document document = new Document(new String(Files.readAllBytes(bndFile.toPath())));
            model.saveChangesTo(document);
   
            String documentContents = document.get();
            m_resourceManager.writeFile(bndFile.toPath(), documentContents.getBytes());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
	 
	}

	private Jar getBaselineJar(String bsn, Version version) {
		try {
			//TODO: Only use baseline or release repo to match bnd baselining behavior
			
	    	Workspace workspace = m_navigator.getCurrentWorkspace();
			
			List<RepositoryPlugin> repositories = workspace.getRepositories();
			SortedMap<Version, RepositoryPlugin> versions = new TreeMap<>();
			for (RepositoryPlugin repo: repositories){
				SortedSet<Version> versions2 = repo.versions(bsn);
				for (Version v : versions2){
					versions.putIfAbsent(v, repo);
				}
			}
			
			if (versions.isEmpty()){
				return null;
			}
			
			RepositoryPlugin repositoryPlugin = versions.get(versions.lastKey());
			
			File file = repositoryPlugin.get(bsn, versions.lastKey(), new HashMap<String, String>() );
			if (file == null){
				System.err.println("Failed to get " + bsn + " with version " + versions.lastKey()
	                    + " skipping baseline of this file!");
				return null;
			}
		
			return new Jar(file);
		
		} catch (Exception e) {
			System.err.println("Failed to get baseline jar for " + bsn  + " skipping baseline of this file! " + e.getMessage());
		}
		return null;
	}

	private List<File> getGeneratedJars() throws Exception {
        List<File> result = new ArrayList<>();
        
        Project project = m_navigator.getCurrentProject();
        if (project != null) {
            result.addAll(addGeneratedJarsFromProject(project));
        } else {
            Collection<Project> projects = m_navigator.getCurrentWorkspace().getAllProjects();
			for (Project p : projects) {
				result.addAll(addGeneratedJarsFromProject(p));
			}
        }
        return result;
    }
    
    private List<File> addGeneratedJarsFromProject(Project project) {
    	File genFolder = project.getTargetDir();
        
        if (genFolder.exists() && genFolder.isDirectory()) {
            File[] jars = genFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().endsWith(".jar");
                }
            });
            return Arrays.asList(jars);
        }
        return Collections.emptyList();
    }
    
    /** A data holder class for the baseline errors. */
    private class BaseLineError {
        String bsn;
        String packageName;
        String newVersion;
        
        @Override
        public String toString() {
            if (packageName != null) {
                return "In " + bsn + " package: " + packageName + " should be " + newVersion;
            }
            return bsn + " should be " + newVersion;
        }
    }
}

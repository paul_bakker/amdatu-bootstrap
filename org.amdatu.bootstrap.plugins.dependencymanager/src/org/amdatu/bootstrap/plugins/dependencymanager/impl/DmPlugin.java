package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.io.File;
import java.util.List;

import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.Command;
import org.amdatu.bootstrap.core.InstallResult;
import org.amdatu.bootstrap.core.Navigator;
import org.amdatu.bootstrap.core.InstallResult.Builder;
import org.amdatu.bootstrap.core.PluginInfo;
import org.amdatu.bootstrap.getopt.Arguments;
import org.amdatu.bootstrap.getopt.Parameters;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
@PluginInfo(name="dependencymanager")
public class DmPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	interface DmInstallParameters extends Parameters {
		boolean useAnnotations();
	}
	
	@Command
	public InstallResult install(DmInstallParameters params) {
		boolean useAnnotations = params.useAnnotations();
		
		InstallResult annotationsInstallResult = null;
		if(useAnnotations) {
			annotationsInstallResult = m_dmService.installAnnotationProcessor();
		}
		
		return InstallResult.builder()
				.addResult(annotationsInstallResult)
				.addResult(m_dmService.addDependencies()).build();
	}
	
	@Arguments(arg = "runconfig...")
	interface DmRunParameters extends Parameters {
		List<File> arguments();
	}
	
	@Command
	public InstallResult run(DmRunParameters params) {
		List<File> runConfigs = params.arguments();
		
		Builder builder = InstallResult.builder();
		
		runConfigs.stream()
			.map(File::toPath)
			.map(m_navigator.getCurrentDir()::resolve)
			.map(m_dmService::addRunDependencies)
			.forEach(builder::addResult);
		
		return builder.build();
	}
}

package org.amdatu.bootstrap.plugins.project;

import org.amdatu.bootstrap.template.BundleTemplateProvider;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.BundleDependency;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Bundle;

@Component(provides=TemplateProvider.class, properties=@Property(name="type", value="project"))
public class ProjectBundleTemplateProvider extends BundleTemplateProvider {

	public static final String PROJECT_TEMPLATE_HEADER = "X-Bootstrap-ProjectTemplate";

	public static final String PROJECT_TEMPLATE_BUNDLE_FILTER = "(" + PROJECT_TEMPLATE_HEADER + "=*)";

	
	public ProjectBundleTemplateProvider() {
		super(PROJECT_TEMPLATE_HEADER);
	}
	
	@Override
	@BundleDependency(filter = PROJECT_TEMPLATE_BUNDLE_FILTER, removed="bundleRemoved")
	protected void bundleAdded(Bundle bundle) {
		super.bundleAdded(bundle);
	}
	
	@Override
	protected void bundleRemoved(Bundle bundle) {
		super.bundleRemoved(bundle);
	}
}

/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.version;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

public interface VersionService {

    /** 
     * Finds a specific version for a bsn, can return an empty list if the bsn is not found *.
     * 
     * @throws Exception when the repository failes to get the versions.
     */
    SortedSet<Version> findVersion(String bsn) throws Exception;

    /** 
     * Return a list of all {@link RepositoryPlugin}'s in the current workspace
     * 
     * @return a list of {@code RepositoryPlugin}'s
     * 
     * @throws IllegalStateException if for example the command was executed in a wrong folder 
     */
    List<RepositoryPlugin> getRepositories();
    
    /**
     * Returns a map that holds all bsn's with all version ranges found. This can be executed in a workspace and then it
     * will navigate into all projects and the value will be a list of 1 (or n) version ranges found for the map key 
     * (bsn). This can also be executed in a project and then will return the version ranges in a list of max 1 entry.
     * 
     * This can return a empty {@link Map} if not in workspace or project
     * 
     * @return a map with as key the bsn and as value a list of all known version (ranges).
     */
    Map<String, Set<String>> getAllDependencies();

    /** 
     * Downloads a jar from the available repository and return its location.
     */
    File downloadVersion(String bsn, String version);

    /**
     * Use a non default repo file for this workspace.
     */
    void setRepositoryFile(File repoFile);
}

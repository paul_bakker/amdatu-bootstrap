package org.amdatu.bootstrap.template;

import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.framework.Bundle;

public class BundleTemplateProvider implements TemplateProvider {

	private final Map<Bundle, Map<String, Template>> m_templates = new HashMap<>();

	private String m_templateHeader;

	public BundleTemplateProvider(String templateHeader) {
		this.m_templateHeader = templateHeader;
	}

	public List<Template> listTemplates() {
		ArrayList<Template> templates = new ArrayList<>();
		for (Map<String, Template> bundleTemplates : m_templates.values()) {
			templates.addAll(bundleTemplates.values());
		}
		return templates;
	}

	public Template get(String name) {
		for (Map<String, Template> bundleTemplates : m_templates.values()) {
			if (bundleTemplates.containsKey(name)) {
				return bundleTemplates.get(name);
			}
		}
		throw new IllegalStateException("Template not found");
	}

	protected void bundleAdded(Bundle bundle) {
		String string = (String)bundle.getHeaders().get(m_templateHeader);
		String[] templateEntries = string.split(",");

		synchronized (m_templates) {

			Map<String, Template> bundleTemplates = m_templates.getOrDefault(bundle, new HashMap<>());
			m_templates.putIfAbsent(bundle, bundleTemplates);
			for (String templateEntry : templateEntries) {
				String[] split = templateEntry.split(";");
				String name = split[1];
				Template template = new Template(name);

				String location = split[0];
				Enumeration<URL> findEntries = bundle.findEntries(location, "*", true);

				while (findEntries.hasMoreElements()) {
					URL url = findEntries.nextElement();
					
					if (url.toString().endsWith("/")){
						continue;
					}
					
					String file = url.getFile();
					String fileName = file.substring(file.indexOf(location) + location.length() + 1);
					template.addResource(new TemplateResource(url, fileName));
				}

				bundleTemplates.put(name, template);
			}
		}		
	}
	
	protected void bundleRemoved(Bundle bundle) {
		synchronized (m_templates) {
			m_templates.remove(bundle);
		}
	}

}

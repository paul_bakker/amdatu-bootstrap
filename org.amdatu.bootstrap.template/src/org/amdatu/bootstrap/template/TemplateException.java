package org.amdatu.bootstrap.template;

/**
 * The exception that is thrown by the template engine if in exception occurs,
 * instead of that exception, or if an other error occurs. Most API methods throw
 * this exception.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TemplateException extends Exception {

    private static final long serialVersionUID = 9067873330488395833L;

    /**
     * Constructs a default TemplateException.
     */
    public TemplateException() {
        super();
    }

    /**
     * Constructs a template exception with the specified string as its message.
     * 
     * @param msg The error message
     */
    public TemplateException(String msg) {
        super(msg);
    }

    /**
     * Constructs a template exception with the specified throwable as the cause of
     * this exception.
     * 
     * @param cause The cause
     */
    public TemplateException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a template exception with the specified string as its message and the
     * throwable as the cause of the exception.
     * 
     * @param msg The message
     * @param cause The cause
     */
    public TemplateException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

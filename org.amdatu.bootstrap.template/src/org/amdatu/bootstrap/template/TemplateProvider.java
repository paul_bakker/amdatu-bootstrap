package org.amdatu.bootstrap.template;

import java.util.List;

public interface TemplateProvider {
	
	List<Template> listTemplates();
}

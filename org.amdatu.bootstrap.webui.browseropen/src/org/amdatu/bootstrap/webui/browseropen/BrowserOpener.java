package org.amdatu.bootstrap.webui.browseropen;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.amdatu.bootstrap.webui.WebBackendConfig;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class BrowserOpener {

	@ServiceDependency
	private volatile WebBackendConfig m_config;
	
	@Start
	public void start() throws IOException, URISyntaxException {
		Desktop.getDesktop().browse(new URI("http://localhost:" + m_config.getPort() + "/ui/index.html"));
	}
}

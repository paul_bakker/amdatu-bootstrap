/**
 * Enables hotkeys
 */
export class HotkeyDirective implements ng.IDirective {        
    
    public restrict = 'A';
    
    public link($scope: ng.IScope, element: JQuery, attributes: ng.IAttributes) {
        $(document).on('keydown', function(event:any) {
            // Don't fire in text-accepting elements
            if ( this !== event.target && (/textarea|select/i.test( event.target.nodeName ) ||
                event.target.type === "text" || $(event.target).prop('contenteditable') == 'true' )) {
                return;
            }
            
            var character = String.fromCharCode(event.which).toLowerCase();
            switch (character) {
                case "p":
                    $('.sidebar input[type=text]').focus();
                    event.preventDefault();        
                    break;
                case "c":
                    $('.main .row:first-child input[type=text]').focus();
                    event.preventDefault();    
                    break;
            }
            return;
        });
    }
    
}
// <reference path="typeScriptDefenitions/libs.d.ts" />

import PluginsService = require('plugins/PluginsService')

class NavigationController {
   static $inject = ['PluginsService'];

    dirs : string[] = [];
    currentDir : string;

    constructor(private pluginsService : PluginsService) {
        //pluginsService.execute("navigation-pwd", {}).subscribe(d => this.currentDir = d);

        pluginsService.ls()
            .map(d => d.substr(d.lastIndexOf("/") + 1))
            .subscribe(d => this.dirs.push(d));
    }

    navigate(dir) {
        this.dirs = [];
        this.pluginsService.navigate(dir);
    }

}

export = NavigationController

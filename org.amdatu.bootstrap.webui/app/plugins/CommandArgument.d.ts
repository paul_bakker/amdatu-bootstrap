interface CommandArgument {
    name : string;
    type : string;
    description : string;
}
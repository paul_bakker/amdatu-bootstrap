// <reference path="typeScriptDefenitions/libs.d.ts" />

class FeedbackModalController {
    static $inject = ['$scope', '$modalInstance', 'question'];


    constructor(private scope : ng.IScope, $modalInstance : ng.ui.bootstrap.IModalServiceInstance, question) {

        console.log("Question: ", question.options);

        scope['question'] = question;
        scope['answer'] = {};

        scope['ok'] = function () {
            console.log('closing', scope['selectedItem']);
            $modalInstance.close(scope['answer'].selectedItem);
        }

    }
}

export = FeedbackModalController